const RolesModel = require("../models/role");

exports.getAllRoles = async () => {
    return await RolesModel.find();
};

exports.getRolesById = async(id) => {
    return await RolesModel.findById(id);
};

exports.createRoles = async(roles) => {
    return await RolesModel.create(roles);
}

exports.updateRoles = async(id, roles) => {
    return await RolesModel.findByIdAndUpdate(id, roles);
};

