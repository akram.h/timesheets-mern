const ResourceModel = require("../models/resources");

exports.getAllResources = async () => {
    return await ResourceModel.find();
};

exports.createResource = async (resource) => {
    return await ResourceModel.create(resource);
};

exports.getResourceById = async (id) => {
    return await ResourceModel.findById(id);
};

exports.updateResource = async(id) => {
    return await ResourceModel.findByIdAndUpdate(id);
};

exports.deleteResource = async(id) => {
    return await ResourceModel.findByIdAndDelete(id);
}
