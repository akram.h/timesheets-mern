var express = require('express');
var router = express.Router();
var resourceController = require('../controllers/resourcesController');

router.get('/', resourceController.getAllResources);
router.post('/addResource',resourceController.addResource);
router.delete('/deleteResource',resourceController.deleteResource);
module.exports = router;