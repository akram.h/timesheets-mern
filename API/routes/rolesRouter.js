var express = require('express');
var router = express.Router();
var roleController = require('../controllers/roleController');

router.get('/',roleController.getAllRoles);

module.exports = router;