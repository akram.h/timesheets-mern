
const mongoose = require('mongoose');

 const timeSheet = {
  _id: mongoose.Schema.Types.ObjectId,
  TimeSheetDate: String,
  Duration: String,
  CustomerId: String,
  ProjectId: String,
  ProjectRoleId: String,
  PhaseId: String,
  ActivityId: String,
  TimeSheetDescription: String,
  Status: String,
  IsBillable: Boolean,
  TimeSheetIsDeleted: Boolean,
  CreatedBy: String,
  CreatedOn: String,
  UpdatedBy: String,
  UpdatedOn: String,
}