const mongoose =  require('mongoose');
const timeSheet = require('./timeSheets');

const ResourceSchema = new mongoose.Schema({
    Name: {
        type: String,
        required: true,
        default: ''
      },
      UserName: {
        type: String,
        required: true,
        default: ''
      },
      MailId: {
        type: String,
        required: true,
        default: ''
      },
      State:{
        type:String,
        required:true,
        default: ''
      },
      Password:{
        type:String,
        default: ''
      },
      RepeatPassword:{
        type:String,
        default: ''
      },
      TimeSheets: [timeSheet],
      CreatedBy:{
        type:String,
        default: ''
      },
      CreatedOn:{
        type:String,
        default: ''
      },
      UpdatedBy:{
        type:String,
        default: ''
      },
      UpdatedOn:{
        type:String,
        default: ''
      },
      RoleId:{
        type:String,
        default: ''
      },
} ,{
  versionKey: false, // Set versionKey option to false
})

module.exports = mongoose.model('Resources',ResourceSchema)