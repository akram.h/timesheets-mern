var RolesModel = require('../models/role');

module.exports = {
    getAllRoles: async (req, res) => {
      try {
        const roles = await RolesModel.find();
        res.json(roles);
      } catch (err) {
        res.status(500).json({ error: err.message });
      }
    },

  };