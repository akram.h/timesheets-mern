const resourceService = require("../services/resourceService");
const roleService = require("../services/roleService");

const ResourceModel = require("../models/resources");

module.exports = { 
  getAllResources: async (req, res) => {
    try {
      const resources = await resourceService.getAllResources();
      const resourceData = [];
  
      if (resources.length > 0) {
        for (const element of resources) {
          if (element.RoleId) {
            try {
              const role = await roleService.getRolesById(element.RoleId);
              if (role !== null) {
                element.RoleId = role.RoleName;
              }
            } catch (roleErr) {
              console.error('Error fetching role:', roleErr);
            }
          }
          resourceData.push(element);
        }
      }
      res.json(resourceData);
    } catch (err) {
      console.error(err, "getAllResources");
      res.status(500).json({ error: err.message });
    }
  },
  
     addResource: async (req, res) => {
      try {
        console.log(req.body);
        const newResource = new ResourceModel();
        newResource.Name = req.body.name;
        newResource.UserName = req.body.userName;
        newResource.MailId = req.body.email;
        newResource.State = req.body.state;
    
        console.log(newResource);
        const resource = await newResource.save();
    
        res.json(resource);
      } catch (err) {
        console.error(err, "addResource");
        res.status(500).json({ error: err.message });
      }
    },

    deleteResource: async (req, res) => {
      try {
        console.log(req._body.id);
        if (req.body != null) {
          const role = await resourceService.deleteResource(req.body.id);
          res.json(role);
        } else {
          console.log(req.body, "delete resource");
        }
      } catch (err) {
        console.error(err, "deleteResource");
        res.status(500).json({ error: err.message });
      }
    }
}