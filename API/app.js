var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var app = express();
const port = process.env.PORT || 8082;
const cors = require('cors');
const bodyParser = require('body-parser');


const connectDB = require('./config/db');
var rolesRouter = require('./routes/rolesRouter');
var resourceRouter = require('./routes/resourcesRoute');
connectDB();

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(cors({
  origin: 'https://www.section.io'
}));

app.use('/role',rolesRouter);
app.use('/resource',resourceRouter);

app.use(bodyParser.json());

app.listen(port, () => console.log(`server running on port ${port}`))

module.exports = app;
