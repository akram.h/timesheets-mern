import React, { useState, useEffect } from 'react';
import { Modal, Button, TextField, Typography } from '@mui/material';
import PropTypes from 'prop-types';

const DynamicFormModal = ({
  open,
  onClose,
  formFields,
  title,
  onSubmit,
  prefilledData,
}) => {
  const [formData, setFormData] = useState( prefilledData || {});
  const [fieldErrors, setFieldErrors] = useState({});
  const fieldToExcludeFromValidation = 'description';

  useEffect(() => {
    if (!open) {
      setFormData({});
      setFieldErrors({});
    }
  }, [open]);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData((prevData) => ({
      ...prevData,
      [name]: value,
    }));
    setFieldErrors((prevErrors) => ({
      ...prevErrors,
      [name]: '',
    }));
  };

  const handleBlur = (e) => {
    const { name, value } = e.target;
    if (name !== fieldToExcludeFromValidation && !value) {
      setFieldErrors({ ...fieldErrors, [name]: 'This field is required.' });
    }
  };

  const handleSubmit = () => {
    const errors = {};
    formFields.forEach((field) => {
      if (field.name !== fieldToExcludeFromValidation && !formData[field.name]) {
        errors[field.name] = 'This field is required.';
      }
    });

    if (Object.keys(errors).length > 0) {
      setFieldErrors(errors);
    } else {
      onSubmit(formData);
      onClose();
    }
  };

  return (
    <Modal
      open={open}
      onClose={onClose}
      style={{
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%',
        height: '100%',
        position: 'relative',
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
      }}
    >
      <div
        style={{
          color: 'blue',
          backgroundColor: 'white',
          width: '35%',
          padding: '20px',
          borderRadius: '8px',
          boxShadow: '0px 2px 10px rgba(0, 0, 0, 0.2)',
          position: 'relative',
        }}
        className="modal-content"
      >
        <Button
          style={{
            position: 'absolute',
            top: '10px',
            right: '10px',
            cursor: 'pointer',
            fontSize: '20px',
            color: 'gray',
          }}
          onClick={onClose}
          variant="text"
        >
          x
        </Button>
        <h2>{title}</h2>
        {formFields.map((field) => (
          <div key={field.name}>
            <TextField
              style={{
                margin: '10px 0',
              }}
              name={field.name}
              label={field.label}
              value={formData[field.name]}
              onChange={handleChange}
              onBlur={handleBlur}
              fullWidth
              {...field.props}
            />
            {fieldErrors[field.name] && (
              <Typography variant="caption" color="error">
                {fieldErrors[field.name]}
              </Typography>
            )}
          </div>
        ))}
        <Button variant="contained" color="primary" onClick={handleSubmit}>
          Submit
        </Button>
      </div>
    </Modal>
  );
};

DynamicFormModal.propTypes = {
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  formFields: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
      label: PropTypes.string.isRequired,
      props: PropTypes.object,
    })
  ).isRequired,
  title: PropTypes.string.isRequired,
  onSubmit: PropTypes.func.isRequired,
  prefilledData: PropTypes.object,
};

export default DynamicFormModal;
