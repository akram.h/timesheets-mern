import { Helmet } from 'react-helmet-async';
import { filter } from 'lodash';
import { sentenceCase } from 'change-case';
import { useEffect, useState } from 'react';
// @mui
import {
  Card,
  Table,
  Stack,
  Paper,
  Avatar,
  Button,
  Popover,
  Checkbox,
  TableRow,
  MenuItem,
  TableBody,
  TableCell,
  Container,
  Typography,
  IconButton,
  TableContainer,
  TablePagination,
} from '@mui/material';
// components
import axios from 'axios';

import Label from '../components/label';
import Iconify from '../components/iconify';
import Scrollbar from '../components/scrollbar';
import DynamicFormModal from '../components/modal';
// sections
import { UserListHead, UserListToolbar } from '../sections/@dashboard/user';
// mock
// import USERLIST from '../_mock/user';
// ----------------------------------------------------------------------

const TABLE_HEAD = [
  { id: 'name', label: 'Name', alignRight: false },
  { id: 'userName', label: 'User Name', alignRight: false },
  { id: 'mailId', label: 'Email Address', alignRight: false },
  { id: 'role', label: 'Role', alignRight: false },
  { id: 'status', label: 'Status', alignRight: false },
  { id: '' },
];

// ----------------------------------------------------------------------

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function applySortFilter(array, comparator, query) {
  // if (!Array.isArray(array)) {
  //   return []; 
  // }

  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });

  if (query) {
    return filter(array, (_user) => _user.name.toLowerCase().indexOf(query.toLowerCase()) !== -1);
  }

  return stabilizedThis.map((el) => el[0]);
}

export default function UserPage() {
  const [open, setOpen] = useState(null);

  const [page, setPage] = useState(0);

  const [order, setOrder] = useState('asc');

  const [selected, setSelected] = useState([]);

  const [orderBy, setOrderBy] = useState('name');

  const [filterName, setFilterName] = useState('');

  const [rowsPerPage, setRowsPerPage] = useState(5);

  const [resources, setResources] = useState([]);

  const [openModal, setOpenModal] = useState(false);

  const [editUserId, setEditUserId] = useState(null);

  const [isEditModalOpen, setIsEditModalOpen] = useState(false);

  const [selectedUserData, setSelectedUserData] = useState(null); // Step 1

  const serverUrl = 'http://localhost:8082/resource';

  useEffect(() => {
    getResources();
  }, []);

  const handleDeleteClick = (id) => {
    axios
      .delete(`${serverUrl}/deleteResource`, {
        data: { id },
        headers: { 'Content-Type': 'application/json' },
      })
      .then((res) => {
        getResources();
        console.log(res, 'resource deleted');
      })
      .catch((error) => {
        console.error(error, 'error deleting resource');
      });
  };


  const getResources = () => {
    axios.get(`${serverUrl}`).then((res) => {
      setResources(res.data);
      console.log(res.data);
    });
  }
 
  const handleResourceSubmit = (formData) => {
    axios.post(`${serverUrl}/addResource`, formData).then((res) => {
      getResources();
    });
  };

  const handleOpenModal = () => {
    setOpenModal(true);
  };

  const handleCloseModal = () => {
    setOpenModal(false);
  };

  const handleEditClick = (user) => {
    setEditUserId(user.id);
    setSelectedUserData(user);
    setIsEditModalOpen(true);
  };

  const ResourceFormFields = [
    { name: 'name', label: 'Name', type: 'text', props: {} },
    { name: 'userName', label: 'User Name', type: 'text', props: {} },
    { name: 'email', label: 'Email Address', type: 'text', props: {} },
    { name: 'state', label: 'Select State', type: 'dropdown', props: {} },
    { name: 'description', label: 'Description', type: 'text', props: {} },
  ];

  const handleOpenMenu = (event) => {
    setOpen(event.currentTarget);
  };

  const handleCloseMenu = () => {
    setOpen(null);
  };

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleSelectAllClick = (event) => {
    if (event.target.checked) {
      const newSelecteds = resources.map((n) => n.Name);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };

  const handleClick = (event, name) => {
    const selectedIndex = selected.indexOf(name);
    let newSelected = [];
    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, name);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(selected.slice(0, selectedIndex), selected.slice(selectedIndex + 1));
    }
    setSelected(newSelected);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setPage(0);
    setRowsPerPage(parseInt(event.target.value, 10));
  };

  const handleFilterByName = (event) => {
    setPage(0);
    setFilterName(event.target.value);
  };

  const emptyRows = page > 0 ? Math.max(0, (1 + page) * rowsPerPage - resources.length) : 0;

  const filteredUsers = applySortFilter(resources, getComparator(order, orderBy), filterName);

  const isNotFound = !filteredUsers.length && !!filterName;

  return (
    <>
  <Helmet>
    <title>TimeSheets</title>
  </Helmet>

  <Container>
    <Stack direction="row" alignItems="center" justifyContent="space-between" mb={5}>
      <Typography variant="h4" gutterBottom>
        Resource
      </Typography>
      <Button variant="contained" startIcon={<Iconify icon="eva:plus-fill" />} onClick={handleOpenModal}>
        New Resource
      </Button>
    </Stack>

    <Card>
      <UserListToolbar numSelected={selected.length} filterName={filterName} onFilterName={handleFilterByName} />

      <Scrollbar>
        <TableContainer sx={{ minWidth: 800 }}>
          <Table>
            <UserListHead
              order={order}
              orderBy={orderBy}
              headLabel={TABLE_HEAD}
              rowCount={resources.length}
              numSelected={selected.length}
              onRequestSort={handleRequestSort}
              onSelectAllClick={handleSelectAllClick}
            />
            <TableBody>
              {filteredUsers.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row) => {
                const selectedUser = selected.indexOf(row.Name) !== -1;
                return (
                  <TableRow hover key={row._id} tabIndex={-1} role="checkbox" selected={selectedUser}>
                    <TableCell padding="checkbox">
                      <Checkbox checked={selectedUser} onChange={(event) => handleClick(event, row.Name)} />
                    </TableCell>
                    <TableCell component="th" scope="row" padding="none">
                      <Stack direction="row" alignItems="center" spacing={2}>
                        <Avatar alt={row.Name} />
                        <Typography variant="subtitle2" noWrap>
                          {row.Name || 'N/A'}
                        </Typography>
                      </Stack>
                    </TableCell>
                    <TableCell align="left">{row.UserName || 'N/A'}</TableCell>
                    <TableCell align="left">{row.MailId || 'N/A'}</TableCell>
                    <TableCell align="left">{row.RoleId || 'N/A'}</TableCell>
                    <TableCell align="left">
                      <Label color={(row.State === 'InActive' && 'error') || 'success'}>
                        {sentenceCase(row.State || 'N/A')}
                      </Label>
                    </TableCell>
                    <TableCell align="right">
                      <IconButton size="large" color="inherit" onClick={handleOpenMenu}>
                        <Iconify icon={'eva:more-vertical-fill'} />
                      </IconButton>
                      <Popover
                        open={Boolean(open)}
                        anchorEl={open}
                        onClose={handleCloseMenu}
                        anchorOrigin={{ vertical: 'top', horizontal: 'left' }}
                        transformOrigin={{ vertical: 'top', horizontal: 'right' }}
                        PaperProps={{
                          sx: {
                            p: 1,
                            width: 140,
                            '& .MuiMenuItem-root': {
                              px: 1,
                              typography: 'body2',
                              borderRadius: 0.75,
                            },
                          },
                        }}
                      >
                        <MenuItem onClick={() => handleEditClick(row)}>
                          <Iconify icon={'eva:edit-fill'} sx={{ mr: 2 }} />
                          Edit
                        </MenuItem>
                        <MenuItem onClick={() => handleDeleteClick(row._id)} sx={{ color: 'error.main' }}>
                          <Iconify icon={'eva:trash-2-outline'} sx={{ mr: 2 }} />
                          Delete
                        </MenuItem>
                      </Popover>
                    </TableCell>
                  </TableRow>
                );
              })}
              {emptyRows > 0 && (
                <TableRow key="emptyRow" style={{ height: 53 * emptyRows }}>
                  <TableCell colSpan={7} />
                </TableRow>
              )}
              {isNotFound && (
                <TableRow key="notFoundRow">
                  <TableCell align="center" colSpan={7} sx={{ py: 3 }}>
                    <Paper
                      sx={{
                        textAlign: 'center',
                      }}
                    >
                      <Typography variant="h6" paragraph>
                        Not found
                      </Typography>
                      <Typography variant="body2">
                        No results were found for the resources.
                        <br /> Please consider adding some resources.
                      </Typography>
                    </Paper>
                  </TableCell>
                </TableRow>
              )}
            </TableBody>
          </Table>
        </TableContainer>
      </Scrollbar>

      <TablePagination
        rowsPerPageOptions={[5, 10, 25]}
        component="div"
        count={resources.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
    </Card>
  </Container>

  <DynamicFormModal
    open={openModal}
    onClose={handleCloseModal}
    formFields={ResourceFormFields}
    title="Add Resource"
    onSubmit={handleResourceSubmit}
  />
  <DynamicFormModal
    open={isEditModalOpen}
    onClose={() => setIsEditModalOpen(false)}
    formFields={ResourceFormFields}
    title="Edit Resource"
    onSubmit={handleResourceSubmit}
    prefilledData={selectedUserData}
  />
</>

  );
}
